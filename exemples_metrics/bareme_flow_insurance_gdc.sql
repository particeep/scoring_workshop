INSERT INTO scoring_metrics (id, created_by, created_at, formula_name, type_signature, formula_code, code_checksum)
    VALUES (
        '4000f269-0da0-4125-bfc4-ccff0361280c',
        'test_flow_928c5516-e56e-4dd3-9ffb-5',
        '2018-04-01T00:00:00Z',
        'BAREME_INSURANCE_GDC',
        '{"q_0_0":"List[String]","q_0_1":"List[String]","q_0_3":"List[String]","q_0_4":"List[String]","q_0_5":"List[String]","q_0_6":"List[String]","q_0_7":"List[String]"}',
        '(input_json: JsValue) => {

             // les activités, déclatation des variables :
             val rep_section4_reponse1 = (input_json \ "q_4_1").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse2 = (input_json \ "q_4_2").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse4 = (input_json \ "q_4_4").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse5 = (input_json \ "q_4_5").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse7 = (input_json \ "q_4_7").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse8 = (input_json \ "q_4_8").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse10 = (input_json \ "q_4_10").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse11 = (input_json \ "q_4_11").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse13 = (input_json \ "q_4_13").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse14 = (input_json \ "q_4_14").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse16 = (input_json \ "q_4_16").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse17 = (input_json \ "q_4_17").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse19 = (input_json \ "q_4_19").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse20 = (input_json \ "q_4_20").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse22 = (input_json \ "q_4_22").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse23 = (input_json \ "q_4_23").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse25 = (input_json \ "q_4_25").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse26 = (input_json \ "q_4_26").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse28 = (input_json \ "q_4_28").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse29 = (input_json \ "q_4_29").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse31 = (input_json \ "q_4_31").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse32 = (input_json \ "q_4_32").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse34 = (input_json \ "q_4_34").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse35 = (input_json \ "q_4_35").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse37 = (input_json \ "q_4_37").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse38 = (input_json \ "q_4_38").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse40 = (input_json \ "q_4_40").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse41 = (input_json \ "q_4_41").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse43 = (input_json \ "q_4_43").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse44 = (input_json \ "q_4_44").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse46 = (input_json \ "q_4_46").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse47 = (input_json \ "q_4_47").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse49 = (input_json \ "q_4_49").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse50 = (input_json \ "q_4_50").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse52 = (input_json \ "q_4_52").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse53 = (input_json \ "q_4_53").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse55 = (input_json \ "q_4_55").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse56 = (input_json \ "q_4_56").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse58 = (input_json \ "q_4_58").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse59 = (input_json \ "q_4_59").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse61 = (input_json \ "q_4_61").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse62 = (input_json \ "q_4_62").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse64 = (input_json \ "q_4_64").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse65 = (input_json \ "q_4_65").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse67 = (input_json \ "q_4_67").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse68 = (input_json \ "q_4_68").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse70 = (input_json \ "q_4_70").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse71 = (input_json \ "q_4_71").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse73 = (input_json \ "q_4_73").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse74 = (input_json \ "q_4_74").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse76 = (input_json \ "q_4_76").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse77 = (input_json \ "q_4_77").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse79 = (input_json \ "q_4_79").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse80 = (input_json \ "q_4_80").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse82 = (input_json \ "q_4_82").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse83 = (input_json \ "q_4_83").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse85 = (input_json \ "q_4_85").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse86 = (input_json \ "q_4_86").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse88 = (input_json \ "q_4_88").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse89 = (input_json \ "q_4_89").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse91 = (input_json \ "q_4_91").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse92 = (input_json \ "q_4_92").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse94 = (input_json \ "q_4_94").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse95 = (input_json \ "q_4_95").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse97 = (input_json \ "q_4_97").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse98 = (input_json \ "q_4_98").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse100 = (input_json \ "q_4_100").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse101 = (input_json \ "q_4_101").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse103 = (input_json \ "q_4_103").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse104 = (input_json \ "q_4_104").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse106 = (input_json \ "q_4_106").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse107 = (input_json \ "q_4_107").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse109 = (input_json \ "q_4_109").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse110 = (input_json \ "q_4_110").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse112 = (input_json \ "q_4_112").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse113 = (input_json \ "q_4_113").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse115 = (input_json \ "q_4_115").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse116 = (input_json \ "q_4_116").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse118 = (input_json \ "q_4_118").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse119 = (input_json \ "q_4_119").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse121 = (input_json \ "q_4_121").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse122 = (input_json \ "q_4_122").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse124 = (input_json \ "q_4_124").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse125 = (input_json \ "q_4_125").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse127 = (input_json \ "q_4_127").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse128 = (input_json \ "q_4_128").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse130 = (input_json \ "q_4_130").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse131 = (input_json \ "q_4_131").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse133 = (input_json \ "q_4_133").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse134 = (input_json \ "q_4_134").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse136 = (input_json \ "q_4_136").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse137 = (input_json \ "q_4_137").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse139 = (input_json \ "q_4_139").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse140 = (input_json \ "q_4_140").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse142 = (input_json \ "q_4_142").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse143 = (input_json \ "q_4_143").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse145 = (input_json \ "q_4_145").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse146 = (input_json \ "q_4_146").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse148 = (input_json \ "q_4_148").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse149 = (input_json \ "q_4_149").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse151 = (input_json \ "q_4_151").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse152 = (input_json \ "q_4_152").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse154 = (input_json \ "q_4_154").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse155 = (input_json \ "q_4_155").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse157 = (input_json \ "q_4_157").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse158 = (input_json \ "q_4_158").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse160 = (input_json \ "q_4_160").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val rep_section4_reponse161 = (input_json \ "q_4_161").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")

             val pourcentache = Map(
               "10%" -> 10d,
               "20%" -> 20d,
               "30%" -> 30d,
               "40%" -> 40d,
               "50%" -> 50d,
               "60%" -> 60d,
               "70%" -> 70d,
               "80%" -> 80d,
               "90%" -> 90d,
               "100%" -> 100d
             )

             var classList = new scala.collection.mutable.ListBuffer[Double]()
             var valueList = new scala.collection.mutable.ListBuffer[Double]()

             if ((pourcentache.getOrElse(rep_section4_reponse1,0d) > 0d)) {classList += 2d ; valueList += pourcentache.getOrElse(rep_section4_reponse1,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse2,0d) > 0d)) {classList += 2d ; valueList += pourcentache.getOrElse(rep_section4_reponse2,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse4,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse4,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse5,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse5,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse7,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse7,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse8,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse8,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse10,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse10,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse11,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse11,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse13,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse13,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse14,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse14,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse16,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse16,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse17,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse17,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse19,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse19,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse20,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse20,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse22,0d) > 0d)) {classList += 4d ; valueList += pourcentache.getOrElse(rep_section4_reponse22,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse23,0d) > 0d)) {classList += 4d ; valueList += pourcentache.getOrElse(rep_section4_reponse23,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse25,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse25,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse26,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse26,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse28,0d) > 0d)) {classList += 3d ; valueList += pourcentache.getOrElse(rep_section4_reponse28,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse29,0d) > 0d)) {classList += 3d ; valueList += pourcentache.getOrElse(rep_section4_reponse29,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse31,0d) > 0d)) {classList += 3d ; valueList += pourcentache.getOrElse(rep_section4_reponse31,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse32,0d) > 0d)) {classList += 3d ; valueList += pourcentache.getOrElse(rep_section4_reponse32,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse34,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse34,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse35,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse35,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse37,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse37,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse38,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse38,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse40,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse40,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse41,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse41,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse43,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse43,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse44,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse44,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse46,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse46,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse47,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse47,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse49,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse49,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse50,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse50,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse52,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse52,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse53,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse53,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse55,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse55,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse56,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse56,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse58,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse58,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse59,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse59,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse61,0d) > 0d)) {classList += 2d ; valueList += pourcentache.getOrElse(rep_section4_reponse61,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse62,0d) > 0d)) {classList += 2d ; valueList += pourcentache.getOrElse(rep_section4_reponse62,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse64,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse64,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse65,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse65,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse67,0d) > 0d)) {classList += 4d ; valueList += pourcentache.getOrElse(rep_section4_reponse67,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse68,0d) > 0d)) {classList += 4d ; valueList += pourcentache.getOrElse(rep_section4_reponse68,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse70,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse70,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse71,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse71,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse73,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse73,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse74,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse74,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse76,0d) > 0d)) {classList += 5d ; valueList += pourcentache.getOrElse(rep_section4_reponse76,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse77,0d) > 0d)) {classList += 5d ; valueList += pourcentache.getOrElse(rep_section4_reponse77,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse79,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse79,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse80,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse80,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse82,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse82,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse83,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse83,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse85,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse85,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse86,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse86,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse88,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse88,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse89,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse89,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse91,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse91,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse92,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse92,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse94,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse94,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse95,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse95,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse97,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse97,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse98,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse98,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse100,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse100,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse101,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse101,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse103,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse103,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse104,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse104,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse106,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse106,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse107,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse107,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse109,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse109,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse110,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse110,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse112,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse112,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse113,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse113,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse115,0d) > 0d)) {classList += 4d ; valueList += pourcentache.getOrElse(rep_section4_reponse115,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse116,0d) > 0d)) {classList += 4d ; valueList += pourcentache.getOrElse(rep_section4_reponse116,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse118,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse118,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse119,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse119,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse121,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse121,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse122,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse122,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse124,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse124,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse125,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse125,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse127,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse127,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse128,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse128,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse130,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse130,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse131,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse131,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse133,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse133,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse134,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse134,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse136,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse136,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse137,0d) > 0d)) {classList += 6d ; valueList += pourcentache.getOrElse(rep_section4_reponse137,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse139,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse139,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse140,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse140,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse142,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse142,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse143,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse143,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse145,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse145,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse146,0d) > 0d)) {classList += 8d ; valueList += pourcentache.getOrElse(rep_section4_reponse146,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse148,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse148,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse149,0d) > 0d)) {classList += 7d ; valueList += pourcentache.getOrElse(rep_section4_reponse149,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse151,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse151,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse152,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse152,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse154,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse154,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse155,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse155,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse157,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse157,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse158,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse158,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse160,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse160,0d)}
             if ((pourcentache.getOrElse(rep_section4_reponse161,0d) > 0d)) {classList += 0d ; valueList += pourcentache.getOrElse(rep_section4_reponse161,0d)}

             val valeur_classmax = if(classList.isEmpty || classList.toList.max == 0) 0d else 1  ///la class la plus élevée
             val valeur_classmin = if (classList.toList.filter(_ > 0).isEmpty) 0d else classList.toList.filter(_ > 0).min /// la classe la plus basse en dehors de zéro => si vide alors 0 sinon la valeur\r+
             val valeur_sommevaleur = if( valueList.toList.sum != 100d) 0d else 1 /// somme les valeurs de pourcentage, on doit faire le check de 100.

             /// variables des cas d exclusion
             val localisation = (input_json \ "q_0_4").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val chiffreAffairesPasse = (input_json \ "q_3_0").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val chiffreAffairesEnCours = (input_json \ "q_3_1").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val chiffreAffairesAnProchain = (input_json \ "q_3_2").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val nbSalariesNonOuvriers = (input_json \ "q_0_6").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val nbSalariesOuvriers = (input_json \ "q_0_7").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val motifResiliation = (input_json \ "q_2_4").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val nbSinitres = (input_json \ "q_2_8").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val montantSinistres = (input_json \ "q_2_9").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")

             /// gestion des cas d exclusion et coeff

             val excl_localisation = localisation match {
               case "En France métropolitaine" => 1d
               case "En département d Outre-Mer (DOM)" => 1.3d
               case "En territoire d Outre-Mer (TOM)" => 0d
               case "En Corse" => 0d
               case "À Monaco" => 0d
               case "En Andorre" => 0d
               case "Autre" => 0d
               case _ => 1d
             }

             val excl_nbSinitres = nbSinitres match {
               case "1" => 1d
               case "2" => 1d
               case "3" => 1.15d
               case "4" => 1.15d
               case "5 et +" => 0d
               case _ => 1d
             }

             val excl_nbSalariesNonOuvriers = nbSalariesNonOuvriers match {
               case "1" => 1L
               case "2" => 2L
               case "3" => 3L
               case "4" => 4L
               case "5" => 5L
               case "6" => 6L
               case "7" => 7L
               case "8" => 8L
               case "9" => 9L
               case "10" => 10L
               case "11 et +" => 11L
               case _ => 0L
             }

             val excl_nbSalariesOuvriers = nbSalariesOuvriers match {
               case "1" => 1L
               case "2" => 2L
               case "3" => 3L
               case "4" => 4L
               case "5" => 5L
               case "6" => 6L
               case "7" => 7L
               case "8" => 8L
               case "9" => 9L
               case "10" => 10L
               case "11 et +" => 11L
               case _ => 0L
             }

             val excl_nbSalaries = if (excl_nbSalariesOuvriers + excl_nbSalariesNonOuvriers > 10L) 0L else 1L

             val excl_motifResiliation = motifResiliation match {
               case "Fausse déclaration" => 0L
               case _ => 1L
             }

             val excl_chiffreAffairesPasse = chiffreAffairesPasse match {
               case "Entre 0 et 32.000€" => 32000L
               case "Entre 32.001€ et 80.000€" => 80000L
               case "Entre 80.001€ et 150.000€" => 150000L
               case "Entre 150.001€ et 200.000€" => 200000L
               case "Entre 200.001€ et 250.000€" => 250000L
               case "Entre 250.001€ et 300.000€" => 300000L
               case "Entre 300.001€ et 350.000€" => 350000L
               case "Entre 350.001€ et 400.000€" => 400000L
               case "Entre 400.001€ et 450.000€" => 450000L
               case "Entre 450.001€ et 500.000€" => 500000L
               case "Entre 500.001€ et 550.000€" => 550000L
               case "Entre 550.001€ et 750.000€" => 750000L
               case "Entre 750.001€ et 2.000.000€" => 2000000L
               case "Plus de 2.000.000€" => 0L
               case _ => 0L
             }

             val excl_chiffreAffairesEnCours = chiffreAffairesEnCours match {
               case "Entre 0 et 32.000€" => 32000L
               case "Entre 32.001€ et 80.000€" => 80000L
               case "Entre 80.001€ et 150.000€" => 150000L
               case "Entre 150.001€ et 200.000€" => 200000L
               case "Entre 200.001€ et 250.000€" => 250000L
               case "Entre 250.001€ et 300.000€" => 300000L
               case "Entre 300.001€ et 350.000€" => 350000L
               case "Entre 350.001€ et 400.000€" => 400000L
               case "Entre 400.001€ et 450.000€" => 450000L
               case "Entre 450.001€ et 500.000€" => 500000L
               case "Entre 500.001€ et 550.000€" => 550000L
               case "Entre 550.001€ et 750.000€" => 750000L
               case "Entre 750.001€ et 2.000.000€" => 2000000L
               case "Plus de 2.000.000€" => 0L
               case _ => 0L
             }

             val excl_chiffreAffairesAnProchain = chiffreAffairesAnProchain match {
               case "Entre 0 et 32.000€" => 32000L
               case "Entre 32.001€ et 80.000€" => 80000L
               case "Entre 80.001€ et 150.000€" => 150000L
               case "Entre 150.001€ et 200.000€" => 200000L
               case "Entre 200.001€ et 250.000€" => 250000L
               case "Entre 250.001€ et 300.000€" => 300000L
               case "Entre 300.001€ et 350.000€" => 350000L
               case "Entre 350.001€ et 400.000€" => 400000L
               case "Entre 400.001€ et 450.000€" => 450000L
               case "Entre 450.001€ et 500.000€" => 500000L
               case "Entre 500.001€ et 550.000€" => 550000L
               case "Entre 550.001€ et 750.000€" => 750000L
               case "Entre 750.001€ et 2.000.000€" => 2000000L
               case "Plus de 2.000.000€" => 0L
               case _ => 0L
             }

             // Prendre le CA le plus élevé
             val chiffreAffaires =  if (excl_chiffreAffairesAnProchain * excl_chiffreAffairesEnCours * excl_chiffreAffairesPasse != 0) {
               val maxAnPasseAnEnCours = java.lang.Math.max(excl_chiffreAffairesPasse, excl_chiffreAffairesEnCours)
               java.lang.Math.max(maxAnPasseAnEnCours, excl_chiffreAffairesAnProchain)
             } else 0L

             //// Fin des cas d exclusion

             // Variables des calculs de coefficients
             val frequenceRepaiement = (input_json \ "q_0_15").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val franchise = (input_json \ "q_0_13").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val repriseDuPasse = (input_json \ "q_0_14").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val dateResiliation_str = (input_json \ "q_2_5").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")
             val sousTraitance = (input_json \ "q_3_5").asOpt[List[String]].getOrElse(List()).headOption.getOrElse("")

             val mapCaClasse = Map(
               (32000L, 1d) -> 4050d,
               (80000L,1d ) -> 4676d,
               (150000L,1d ) -> 5093d,
               (200000L,1d ) -> 6137d,
               (250000L,1d ) -> 7180d,
               (300000L,1d ) -> 8224d,
               (350000L,1d ) -> 9266d,
               (400000L,1d ) -> 10310d,
               (450000L,1d ) -> 11353d,
               (500000L,1d ) -> 12397d,
               (550000L,1d ) -> 13439d,
               (750000L,1d ) -> 14483d,
               (2000000L,1d ) -> 14483d,
               (32000L,2d ) -> 3645d,
               (80000L,2d ) -> 4209d,
               (150000L,2d ) -> 4585d,
               (200000L,2d ) -> 5524d,
               (250000L,2d ) -> 6461d,
               (300000L,2d ) -> 7401d,
               (350000L,2d ) -> 8341d,
               (400000L,2d ) -> 9279d,
               (450000L,2d ) -> 10218d,
               (500000L,2d ) -> 11157d,
               (550000L,2d ) -> 12097d,
               (750000L,2d ) -> 13035d,
               (2000000L,2d ) -> 13035d,
               (32000L,3d ) -> 3513d,
               (80000L,3d ) -> 4053d,
               (150000L,3d ) -> 4415d,
               (200000L,3d ) -> 5320d,
               (250000L,3d ) -> 6223d,
               (300000L,3d ) -> 7128d,
               (350000L,3d ) -> 8031d,
               (400000L,3d ) -> 8936d,
               (450000L,3d ) -> 9840d,
               (500000L,3d ) -> 10745d,
               (550000L,3d ) -> 11648d,
               (750000L,3d ) -> 12553d,
               (2000000L,3d ) -> 12553d,
               (32000L,4d ) -> 3141d,
               (80000L,4d ) -> 3741d,
               (150000L,4d ) -> 4076d,
               (200000L,4d ) -> 4910d,
               (250000L,4d ) -> 5745d,
               (300000L,4d ) -> 6579d,
               (350000L,4d ) -> 7414d,
               (400000L,4d ) -> 8249d,
               (450000L,4d ) -> 9083d,
               (500000L,4d ) -> 9918d,
               (550000L,4d ) -> 10752d,
               (750000L,4d ) -> 11587d,
               (2000000L,4d ) -> 11587d,
               (32000L,5d ) -> 2701d,
               (80000L,5d ) -> 3119d,
               (150000L,5d ) -> 3397d,
               (200000L,5d ) -> 4092d,
               (250000L,5d ) -> 4788d,
               (300000L,5d ) -> 5483d,
               (350000L,5d ) -> 6179d,
               (400000L,5d ) -> 6874d,
               (450000L,5d ) -> 7570d,
               (500000L,5d ) -> 8265d,
               (550000L,5d ) -> 8961d,
               (750000L,5d ) -> 9656d,
               (2000000L,5d ) -> 9656d,
               (32000L,6d ) -> 2701d,
               (80000L,6d ) -> 3119d,
               (150000L,6d ) -> 3397d,
               (200000L,6d ) -> 4092d,
               (250000L,6d ) -> 4788d,
               (300000L,6d ) -> 5483d,
               (350000L,6d ) -> 6179d,
               (400000L,6d ) -> 6874d,
               (450000L,6d ) -> 7570d,
               (500000L,6d ) -> 8265d,
               (550000L,6d ) -> 8961d,
               (750000L,6d ) -> 9656d,
               (2000000L,6d ) -> 9656d,
               (32000L,7d ) -> 1867d,
               (80000L,7d ) -> 2284d,
               (150000L,7d ) -> 2701d,
               (200000L,7d ) -> 3119d,
               (250000L,7d ) -> 3397d,
               (300000L,7d ) -> 4092d,
               (350000L,7d ) -> 4788d,
               (400000L,7d ) -> 5483d,
               (450000L,7d ) -> 6179d,
               (500000L,7d ) -> 6874d,
               (550000L,7d ) -> 7570d,
               (750000L,7d ) -> 8405d,
               (2000000L,7d ) -> 8405d,
               (32000L,8d ) -> 1171d,
               (80000L,8d ) -> 1310d,
               (150000L,8d ) -> 1589d,
               (200000L,8d ) -> 2145d,
               (250000L,8d ) -> 2701d,
               (300000L,8d ) -> 3397d,
               (350000L,8d ) -> 4092d,
               (400000L,8d ) -> 4788d,
               (450000L,8d ) -> 5483d,
               (500000L,8d ) -> 6179d,
               (550000L,8d ) -> 6874d,
               (750000L,8d ) -> 7570d,
               (2000000L,8d ) -> 7570d
             )

             val primeDeBase = mapCaClasse.getOrElse((chiffreAffaires,valeur_classmin),0d) * valeur_classmax * valeur_sommevaleur

             val coeff_frequenceRepaiement = frequenceRepaiement match {
               case "Annuel" => 1d
               case "Semestriel" => 1.03d
               case "Trimestriel" => 1.04d
               case _ => 1d
             }

             val split_frequenceRepaiement = frequenceRepaiement match {
               case "Annuel" => 1d
               case "Semestriel" => 0.5d
               case "Trimestriel" => 0.25d
               case _ => 1d
             }

             val coeff_franchise = franchise match {
               case "1 000€" => 1.1d
               case "1 500€" => 1d
               case "2 000€" => 0.9d
               case "3 000€" => 0.8d
               case _ => 1d
             }

             val coeff_repriseDuPasse = repriseDuPasse match {
               case "Oui" => 1.15d
               case "Non" => 1d
               case _ => 1d
             }

             val coeff_dateResiliation = {
               val parser = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd''T''HH:mm:ssZ")
               val today = java.time.LocalDateTime.now()
               val dateResiliation = scala.util.Try {
                 java.time.LocalDateTime.parse(dateResiliation_str, parser)
               }.getOrElse(today)

               val days_between = java.time.temporal.ChronoUnit.DAYS.between(dateResiliation, today)

               if (days_between >= 0 && days_between <= 789)
                 1d
               else if (days_between >= 790 && days_between <= 1825)
                 1.15d
               else if (days_between > 1825)
                 1.15d
               else
                 1d
             }

             val coeff_sousTraitance = sousTraitance match {
               case "0%" => 1d
               case _ => 1.3d
             }

             val fraisSouscription = 200L
             val fraisCourtage = 150L

             val primeToPay = ((primeDeBase *
               coeff_sousTraitance *
               coeff_dateResiliation *
               coeff_repriseDuPasse *
               coeff_franchise *
               split_frequenceRepaiement *
               coeff_frequenceRepaiement *
               excl_motifResiliation *
               excl_nbSalaries *
               excl_nbSinitres *
               excl_localisation) * 100)
               .toLong

             JsObject(Map("score" -> JsNumber(primeToPay)))
           }',
        'c2701ff4ecf814acf3540f4d079d21e4'
      );
