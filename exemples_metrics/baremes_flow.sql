-- key test: test_flow_928c5516-e56e-4dd3-9ffb-5

-- /////////////////////// lookup /////////////////////////////

SELECT id, created_by, formula_name, SUBSTR(formula_code,0,20) as code, code_checksum FROM scoring_metrics ;

-- /////////////////////// modifs pour I/O JsValue //////////////////////

-- maj pour flow1
UPDATE scoring_metrics SET formula_code = '(input_json: JsValue) => {
val answer0 = (input_json \ "q_0_0").validate[List[String]].get
val answer1 = (input_json \ "q_0_1").validate[List[String]].get
val answer3 = (input_json \ "q_0_3").validate[List[String]].get
val answer4 = (input_json \ "q_0_4").validate[List[String]].get
val q0_points = answer0.map{ ans:String => ans match {
  case "Moins de 30.000€" => 4L
  case "Entre 30.000€ et 60.000€" => 8L
  case "Entre 60.000€ et 100.000€" => 12L
  case "Plus de 100.000€" => 18L
  case _ => 0L
}}.sum
val q1_number_str = answer1(0).replace(" ","").replace("k€","000").replace("€","").replace(".","").replace(",","")
val q1_points = math.min(50L, try {(q1_number_str.toLong / 10000.0).round} catch {case _: Throwable => 0L})
val q3_points = answer3.map{ ans:String => ans match {
  case "Entre 5 et 10 ans" => 4L
  case "Entre 10 et 15 ans" => 12L
  case "Plus de 15 ans" => 16L
  case _ => 0L
}}.sum
val q4_points = answer4.map { ans:String => ans match {
  case "Salaire/Retraite" => 12L
  case "Revenus immobiliers" => 16L
  case "Dividendes" => 14L
  case "Autres" => 8L
  case _ => 0L
}}.sum
// q2 sera hors scoring
val score = q0_points +  q1_points + q3_points + q4_points
JsObject(Map("score" -> JsNumber(score)))
}'
WHERE id = '927a79bc-a959-4e54-8d92-c8833098639a' ;

UPDATE scoring_metrics SET code_checksum = 'e1e14290a82a9874995ef2adfb640a3e'
WHERE id = '927a79bc-a959-4e54-8d92-c8833098639a' ;

-- maj pour flow2
UPDATE scoring_metrics SET formula_code = '(input_json: JsValue) => {
val answer0 = (input_json \ "q_0_0").validate[List[String]].get
val answer1 = (input_json \ "q_0_1").validate[List[String]].get
val answer2 = (input_json \ "q_0_2").validate[List[String]].get
val answer3 = (input_json \ "q_0_3").validate[List[String]].get

val now = ZonedDateTime.now()
val zero_time = " 00:00 Europe/Paris"
val date_format = java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm VV")
val q1_real_date = try {ZonedDateTime.parse(answer1.head + zero_time, date_format)} catch {case _: Throwable => ZonedDateTime.now()}
val q1_years = math.max(now.getYear - q1_real_date.getYear, 0)
val q1_points = math.min(30, q1_years*2)

val q0_points = answer0.map{ ans:String => ans match {
  case "Gel" => 5L
  case "Sanction" => 0L
  case "Surveillance" => 10L
  case _ => 0L
}}.sum
val q2_points = answer2.map{ ans:String => ans match {
  case "Privé" => 5L
  case "Professionnel" => 20L
  case _ => 0L
}}.sum
val q3_points = answer3.map { ans:String => ans match {
  case "<50K€" => 10L
  case "entre 50k€ et 100k€" => 20L
  case ">100k€" => 40L
  case _ => 0L
}}.sum
val score = q0_points + q1_points + q2_points + q3_points
JsObject(Map("score" -> JsNumber(score)))
}'
WHERE id = '4b5a450b-366d-415d-b0f0-34a18163fe16' ;


UPDATE scoring_metrics SET code_checksum = 'd65194cc1cf7543809ce0870e29199c6'
WHERE id = '4b5a450b-366d-415d-b0f0-34a18163fe16' ;


-- màj pour flow4
UPDATE scoring_metrics SET formula_code =  '(input_json: JsValue) => {
val base_premium = 100L
val answer0 = (input_json \ "q_0_0").validate[List[String]].get.headOption
val answer1 = (input_json \ "q_0_1").validate[List[String]].get.headOption
val answer3 = (input_json \ "q_0_3").validate[List[String]].get.headOption
val answer4 = (input_json \ "q_0_4").validate[List[String]].get.headOption
val answer5 = (input_json \ "q_0_5").validate[List[String]].get.headOption
val answer6 = (input_json \ "q_0_6").validate[List[String]].get.headOption
val answer7 = (input_json \ "q_0_7").validate[List[String]].get.headOption
val q0_points = answer0 match {
  case Some("Principale") => 20L
  case Some("Secondaire") => 10L
  case _ => 0L
}

val q1_points = answer1 match {
  case Some("un appartement") => 10L
  case Some("une maison") => 40L
  case _ => 0L
}

val q3_points = answer3 match {
  case Some("1") => 0L
  case Some("2") => 40L
  case Some("3") => 60L
  case Some("4") => 100L
  case Some("5 et +") => 300L
  case _ => 0L
}

val q4_points = answer4 match {
  case Some("moins de 40m2") => -10L
  case Some("plus de 40m2") => 20L
  case _ => 0L
}

val q5_points = answer5 match {
  case Some("Après 2012") => -10L
  case Some("2006 à 2012") => 20L
  case Some("2000 à 2005") => 30L
  case Some("1990 à 1999") => 40L
  case Some("1974 à 1989") => 70L
  case Some("Avant 1950") => 150L
  case _ => 0L
}

val q6_points = answer6 match {
  case Some("Moins de 6 mois") => 10L
  case Some("Plus de 6 mois") => -10L
  case _ => 0L
}

val answer7_num: Long = try { answer7.get.toLong } catch {case _: Throwable => 0L}
val q7_points = (answer7_num / 5000.0).round

val quotation = base_premium + q0_points + q1_points + q3_points + q4_points + q5_points + q6_points + q7_points

JsObject(Map("score" -> JsNumber(quotation)))
}'
WHERE id = 'e7ba2f75-a6e1-44cc-bb72-513744836a95' ;


UPDATE scoring_metrics SET code_checksum = '7e57f1fb56b183e4f23d7fd192519d6f'
WHERE id = 'e7ba2f75-a6e1-44cc-bb72-513744836a95' ;











-- //////////////////// original pour flow4 //////////////////////
-- # "PARTICEEP__2c1fac95-fd4f-46dd-a1ff-" "3201fd2a-d2e2-434d-a04c-76e3cda523d3"
-- FORM correspondant f3862cb9-6fe4-4dca-a1c4-8bbea570c037
-- FORM local cfe139cd-122f-427a-bb93-3d5292aa4072
-- SIGN doc 0e5b4220-c0b5-4894-bc8d-4d4f2584ce7b
INSERT INTO scoring_metrics (id, created_by, created_at, formula_name, type_signature, formula_code, code_checksum)
    VALUES (
        'e7ba2f75-a6e1-44cc-bb72-513744836a95',
        'PARTICEEP__2c1fac95-fd4f-46dd-a1ff-',
        '2018-01-01T00:00:00Z',
        'BAREME_DEMO_INSURANCE',
        '{"q_0_0":"List[String]","q_0_1":"List[String]","q_0_3":"List[String]","q_0_4":"List[String]","q_0_5":"List[String]","q_0_6":"List[String]","q_0_7":"List[String]"}',
        '(input_json: JsValue) => {
val base_premium = 100L
val answer0 = (input_json \ "q_0_0").validate[List[String]].get.headOption
val answer1 = (input_json \ "q_0_1").validate[List[String]].get.headOption
val answer3 = (input_json \ "q_0_3").validate[List[String]].get.headOption
val answer4 = (input_json \ "q_0_4").validate[List[String]].get.headOption
val answer5 = (input_json \ "q_0_5").validate[List[String]].get.headOption
val answer6 = (input_json \ "q_0_6").validate[List[String]].get.headOption
val answer7 = (input_json \ "q_0_7").validate[List[String]].get.headOption
val q0_points = answer0 match {
  case Some("Principale") => 20L
  case Some("Secondaire") => 10L
  case _ => 0L
}

val q1_points = answer1 match {
  case Some("un appartement") => 10L
  case Some("une maison") => 40L
  case _ => 0L
}

val q3_points = answer3 match {
  case Some("1") => 0L
  case Some("2") => 40L
  case Some("3") => 60L
  case Some("4") => 100L
  case Some("5 et +") => 300L
  case _ => 0L
}

val q4_points = answer4 match {
  case Some("moins de 40m2") => -10L
  case Some("plus de 40m2") => 20L
  case _ => 0L
}

val q5_points = answer5 match {
  case Some("Après 2012") => -10L
  case Some("2006 à 2012") => 20L
  case Some("2000 à 2005") => 30L
  case Some("1990 à 1999") => 40L
  case Some("1974 à 1989") => 70L
  case Some("Avant 1950") => 150L
  case _ => 0L
}

val q6_points = answer6 match {
  case Some("Moins de 6 mois") => 10L
  case Some("Plus de 6 mois") => -10L
  case _ => 0L
}

val answer7_num: Long = try { answer7.get.toLong } catch {case _: Throwable => 0L}
val q7_points = (answer7_num / 5000.0).round

val quotation = base_premium + q0_points + q1_points + q3_points + q4_points + q5_points + q6_points + q7_points

JsObject(Map("score" -> JsNumber(quotation)))
}',
        '7e57f1fb56b183e4f23d7fd192519d6f'
      );

-- //////////////////// classique pour flow4 //////////////////////
INSERT INTO scoring_metrics (id, created_by, created_at, formula_name, type_signature, formula_code, code_checksum)
    VALUES (
        'e7ba2f75-a6e1-44cc-bb72-513744836a95',
        'PARTICEEP__2c1fac95-fd4f-46dd-a1ff-',
        '2018-01-01T00:00:00Z',
        'BAREME_DEMO_INSURANCE',
        '{"q_0_0":"List[String]","q_0_1":"List[String]","q_0_3":"List[String]","q_0_4":"List[String]","q_0_5":"List[String]","q_0_6":"List[String]","q_0_7":"List[String]"}',
        '(infos: Input) => {
val base_premium = 100L

val q0_points = infos.q_0_0.headOption match {
  case Some("Principale") => 20L
  case Some("Secondaire") => 10L
  case _ => 0L
}

val q1_points = infos.q_0_1.headOption match {
  case Some("un appartement") => 10L
  case Some("une maison") => 40L
  case _ => 0L
}

val q3_points = infos.q_0_3.headOption match {
  case Some("1") => 0L
  case Some("2") => 40L
  case Some("3") => 60L
  case Some("4") => 100L
  case Some("5 et +") => 300L
  case _ => 0L
}

val q4_points = infos.q_0_4.headOption match {
  case Some("moins de 40m2") => -10L
  case Some("plus de 40m2") => 20L
  case _ => 0L
}

val q5_points = infos.q_0_5.headOption match {
  case Some("Après 2012") => -10L
  case Some("2006 à 2012") => 20L
  case Some("2000 à 2005") => 30L
  case Some("1990 à 1999") => 40L
  case Some("1974 à 1989") => 70L
  case Some("Avant 1950") => 150L
  case _ => 0L
}

val q6_points = infos.q_0_6.headOption match {
  case Some("Moins de 6 mois") => 10L
  case Some("Plus de 6 mois") => -10L
  case _ => 0L
}

val answer7_num: Long = try { infos.q_0_7.head.toLong } catch {case _: Throwable => 0L}
val q7_points = (answer7_num / 5000.0).round

base_premium + q0_points + q1_points + q3_points + q4_points + q5_points + q6_points + q7_points
}',
        '5a91378ec6746cfd096cb6aa3a6f520e'
      );

-- //////////////////// originaux I/O via Input classique //////////////////////

INSERT INTO scoring_metrics (id, created_by, created_at, formula_name, type_signature, formula_code, code_checksum)
    VALUES (
        '927a79bc-a959-4e54-8d92-c8833098639a',
        'PARTICEEP__cb6be0f9-6795-4f62-b350-',
        '2018-01-01T00:00:00Z',
        'BAREME_DEMO_FLOW',
        '{"q_0_0":"List[String]","q_0_1":"List[String]","q_0_3":"List[String]","q_0_4":"List[String]"}',
        '(infos: Input) => {
val q0_points = infos.q_0_0.map{ ans:String => ans match {
  case "Moins de 30.000€" => 4L
  case "Entre 30.000€ et 60.000€" => 8L
  case "Entre 60.000€ et 100.000€" => 12L
  case "Plus de 100.000€" => 18L
  case _ => 0L
}}.sum
val q1_number_str = infos.q_0_1(0).replace(" ","").replace("k€","000").replace("€","").replace(".","").replace(",","")
val q1_points = math.min(50L, try {(q1_number_str.toLong / 10000.0).round} catch {case _: Throwable => 0L})
val q3_points = infos.q_0_3.map{ ans:String => ans match {
  case "Entre 5 et 10 ans" => 4L
  case "Entre 10 et 15 ans" => 12L
  case "Plus de 15 ans" => 16L
  case _ => 0L
}}.sum
val q4_points = infos.q_0_4.map { ans:String => ans match {
  case "Salaire/Retraite" => 12L
  case "Revenus immobiliers" => 16L
  case "Dividendes" => 14L
  case "Autres" => 8L
  case _ => 0L
}}.sum
// q2 sera hors scoring
q0_points +  q1_points + q3_points + q4_points
}',
        '85ce7c8e776db598f14d885a09cc2f88'
      );

-- bareme for DEMO2
-- ==============
INSERT INTO scoring_metrics (id, created_by, created_at, formula_name, type_signature, formula_code, code_checksum)
    VALUES (
        '4b5a450b-366d-415d-b0f0-34a18163fe16',
        'PARTICEEP__985d17a2-a334-4f1b-89fe-',
        '2018-01-01T00:00:00Z',
        'BAREME_DEMO2_FLOW_ETP',
        '{"q_0_0":"List[String]","q_0_1":"List[String]","q_0_2":"List[String]","q_0_3":"List[String]"}',
        '(infos: Input) => {
val now = ZonedDateTime.now()
val zero_time = " 00:00 Europe/Paris"
val date_format = java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm VV")
val q1_real_date = try {ZonedDateTime.parse(infos.q_0_1.head + zero_time, date_format)} catch {case _: Throwable => ZonedDateTime.now()}
val q1_years = math.max(now.getYear - q1_real_date.getYear, 0)
val q1_points = math.min(30, q1_years*2)

val q0_points = infos.q_0_0.map{ ans:String => ans match {
  case "Gel" => 5L
  case "Sanction" => 0L
  case "Surveillance" => 10L
  case _ => 0L
}}.sum
val q2_points = infos.q_0_2.map{ ans:String => ans match {
  case "Privé" => 5L
  case "Professionnel" => 20L
  case _ => 0L
}}.sum
val q3_points = infos.q_0_3.map { ans:String => ans match {
  case "<50K€" => 10L
  case "entre 50k€ et 100k€" => 20L
  case ">100k€" => 40L
  case _ => 0L
}}.sum
q0_points + q1_points + q2_points + q3_points
}',
        'e97857d1226529712b86d38e2449c02d'
      );
