
UPDATE consumer SET scoring_service = '{"internal_user_form_metric_id": "00d43222-d726-43ad-ab85-96cfe00cc489"}' WHERE name = 'testngle2' ;

-- traitement DeepForm entier (et du coup pas de type_signature)
INSERT INTO scoring_metrics (id,created_by,created_at,formula_name,formula_code,code_checksum)
    VALUES (
      '00d43222-d726-43ad-ab85-96cfe00cc489',
      'testngle2_dd719c89-c75a-405f-aebd-e',
      '2018-03-01T12:00:00Z',
      'BAREME FORMDEEP',
      '(form_deep_json: JsValue) => {
  val questions = (form_deep_json \\ "questions").flatMap(_.as[Seq[Map[String,JsValue]]])
  val all_points: Seq[Long] = questions.flatMap{
    js_q => {
      val q_type = js_q("question_type").as[String]
      q_type match {
        case "RADIO"|"SELECT"|"CHECKBOX" => {
          val psblts = js_q("possibilities").as[Seq[Map[String,JsValue]]]
          val weights = psblts.map{ p => p("label").as[String] -> p.getOrElse("weight", JsNumber(0)).as[Long] }.toMap
          js_q("answers").as[Seq[String]].map{
            user_answer => weights.getOrElse(user_answer, 0L)
          }
        }
        case _ => Seq(0L)
      }
    }
  }
  val score = all_points.sum
  JsObject(Map("score" -> JsNumber(score)))
},
      '57591c3f7de54011322ac05629daf2b8'
    );

-- idem avec logging
INSERT INTO scoring_metrics (id,created_by,created_at,formula_name,formula_code,code_checksum)
    VALUES (
      '00d43222-d726-43ad-ab85-96cfe00cc489',
      'testngle2_dd719c89-c75a-405f-aebd-e',
      '2018-03-01T12:00:00Z',
      'BAREME FORMDEEP',
      '(form_deep_json: JsValue) => {
  val questions = (form_deep_json \\ "questions").flatMap(_.as[Seq[Map[String,JsValue]]])
  val all_points: Seq[Long] = questions.flatMap{
    js_q => {
      val q_type = js_q("question_type").as[String]

      println("q_type:" + q_type)

      q_type match {
        case "RADIO"|"SELECT"|"CHECKBOX" => {
          val psblts = js_q("possibilities").as[Seq[Map[String,JsValue]]]
          val weights = psblts.map{ p => p("label").as[String] -> p("weight").as[Long] }.toMap

          println("\nweights:" + weights)

          js_q("answers").as[Seq[String]].map{
            user_answer => weights.getOrElse(user_answer, 0L)
          }
        }
        case _ => Seq(0L)
      }
    }
  }

  println("all_points:" + all_points )

  val score = all_points.sum
  JsObject(Map("score" -> JsNumber(score)))
}',
      '6886ed71568a7f95eb21249cdaa999f5'
    );



-- somme des poids sur compact form dans particeep-flow (cf. dans flow la fonction compactFormAnswers)
INSERT INTO scoring_metrics (id,created_by,created_at,formula_name,type_signature,formula_code,code_checksum)
    VALUES (
        '00d43222-d726-43ad-ab85-96cfe00cc487',
        'testngle2_dd719c89-c75a-405f-aebd-e',
        '2018-01-01T00:00:00Z',
        'BAREME CHOIX MULTIPLES',
        '{"all_answers":"List[List[String]]", "possibilities_weights":"Map[String, Long]"}',
        '(input_json: JsValue) => {
val all_answers = (input_json \ "all_answers").validate[List[List[String]]].get
val weights = (input_json \ "possibilities_weights").validate[Map[String, Long]].get

  val score = all_answers.flatMap{
    (q_answers: List[String]) => q_answers.map {
      (an_answer:String) => weights.getOrElse(an_answer, 0L)
    }
  }.sum

  JsObject(Map("score" -> JsNumber(score)))
}',
        'f145aeccb853289701a0bf8e4a74aa0e'
      );


-- vérification des checksum MD5
SELECT id, created_by, formula_name, md5(formula_code) , code_checksum FROM scoring_metrics ;

-- exemple de mise à jour du checksum MD5
UPDATE scoring_metrics SET code_checksum = '6886ed71568a7f95eb21249cdaa999f5' WHERE id = '00d43222-d726-43ad-ab85-96cfe00cc489' ;


