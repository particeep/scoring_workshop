package minimetric


// USAGE > run "{\"argname1\":[1,2,4], \"argname2\":true}"
// =======================================================
import play.api.libs.json._
import play.api.libs.functional.syntax._
import java.time.ZonedDateTime
import java.time.LocalDate


object MainMetricRun extends App {

  println("run")

  //                              insert metric code here
  // ------------------------------------|-->
  val test_function: JsValue => JsValue = (input_json: JsValue) => {
    val questions = (input_json \\ "questions").flatMap(_.as[Seq[Map[String,JsValue]]])
    val all_points: Seq[Long] = questions.flatMap{
      js_q => {
        val q_type = js_q("question_type").as[String]
        q_type match {
          case "RADIO"|"SELECT"|"CHECKBOX" => {
            val psblts = js_q("possibilities").as[Seq[Map[String,JsValue]]]
            val weights = psblts.map{p => p("label").as[String] -> p("weight").as[Long]}.toMap
            js_q("answers").as[Seq[String]].map{
              user_answer => weights.getOrElse(user_answer, 0L)
            }
          }
          case _ => Seq(0L)
        }
      }
    }
    val score = all_points.sum
    JsObject(Map("score" -> JsNumber(score)))
  }
  // <------ end of metric code

  val res: JsValue = test_function.apply(Json.parse(args(0)).validate[JsValue].get)

  println("out==/" + Json.toJson(res))

}



// --------------------------------------------------
// other run possibility on an inline exemple_input:
// --------------------------------------------------
// val res: JsValue = test_function.apply(Json.parse(exemple_input).validate[JsValue].get)


//val exemple_input =
//"""{
//  |  "id": "394b8390-071c-443b-974a-f8f4d99a06a9",
//  |  "created_at": "2018-03-27T09:45:22Z",
//  |  "name": "nitform",
//  |  "description": "natural investor test form",
//  |  "tag": "NATURAL",
//  |  "last_updated_at": "2018-03-28T15:02:06Z",
//  |  "sections": [
//  |    {
//  |      "id": "7e620d12-d0fb-4532-8273-8573b948a600",
//  |      "created_at": "2018-03-27T09:45:24Z",
//  |      "form_id": "394b8390-071c-443b-974a-f8f4d99a06a9",
//  |      "name": "Section",
//  |      "index": 0,
//  |      "done": true,
//  |      "questions": [
//  |        {
//  |          "id": "f43deb75-8b3c-48e2-8508-e2307fdd7ba2",
//  |          "created_at": "2018-03-27T09:45:24Z",
//  |          "section_id": "7e620d12-d0fb-4532-8273-8573b948a600",
//  |          "label": "Agree ?",
//  |          "question_type": "RADIO",
//  |          "required": true,
//  |          "index": 0,
//  |          "answers": [
//  |            "Yes (5 points)"
//  |          ],
//  |          "possibilities": [
//  |            {
//  |              "id": "46ce2f28-6dea-4d5a-ac42-f201cee73d79",
//  |              "created_at": "2018-03-27T09:45:24Z",
//  |              "question_id": "f43deb75-8b3c-48e2-8508-e2307fdd7ba2",
//  |              "label": "Yes (5 points)",
//  |              "index": 0,
//  |              "weight": 5
//  |            },
//  |            {
//  |              "id": "414a13b5-5c7d-433a-b027-b97853be3831",
//  |              "created_at": "2018-03-27T09:45:41Z",
//  |              "question_id": "f43deb75-8b3c-48e2-8508-e2307fdd7ba2",
//  |              "label": "No (4 points)",
//  |              "index": 1,
//  |              "weight": 4
//  |            },
//  |            {
//  |              "id": "263e38c3-fac1-4209-a875-ca342e77e2e2",
//  |              "created_at": "2018-03-28T13:59:19Z",
//  |              "question_id": "f43deb75-8b3c-48e2-8508-e2307fdd7ba2",
//  |              "label": "maybe (3 points)",
//  |              "index": 2,
//  |              "weight": 3
//  |            }
//  |          ]
//  |        },
//  |        {
//  |          "id": "358c55b1-aa9f-41e9-9366-5154ab31c0e6",
//  |          "created_at": "2018-03-28T13:59:32Z",
//  |          "section_id": "7e620d12-d0fb-4532-8273-8573b948a600",
//  |          "label": "checkbox exemple",
//  |          "question_type": "CHECKBOX",
//  |          "required": false,
//  |          "index": 1,
//  |          "answers": [
//  |            "a (2 points)",
//  |            "b (1 point)"
//  |          ],
//  |          "possibilities": [
//  |            {
//  |              "id": "ee681d2b-34a9-4aea-b289-880895ec84b5",
//  |              "created_at": "2018-03-28T13:59:36Z",
//  |              "question_id": "358c55b1-aa9f-41e9-9366-5154ab31c0e6",
//  |              "label": "a (2 points)",
//  |              "index": 0,
//  |              "weight": 2
//  |            },
//  |            {
//  |              "id": "588050c0-d5af-469b-986d-d2ffe609a58e",
//  |              "created_at": "2018-03-28T13:59:52Z",
//  |              "question_id": "358c55b1-aa9f-41e9-9366-5154ab31c0e6",
//  |              "label": "b (1 point)",
//  |              "index": 1,
//  |              "weight": 1
//  |            }
//  |          ]
//  |        }
//  |      ]
//  |    }
//  |  ]
//  |}
//""".stripMargin
