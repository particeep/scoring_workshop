Mini project to test scoring_metrics
=====================================

## Some exemple of metrics

Metrics are scala code functions that have to be added in SQL, so they are presented as SQL INSERT statements.

You can find them in the `exemples_metrics` folder.

## Scala formula format
A metric formula code must respect the following structure:
```scala
(blablabla: JsValue) => {
    yourReturnJsValue
}
```
For particeep-eval, this formula is the only requirement.

For the API's scoring_metrics module, there is an additional requirement:
  - the return JsValue should additionnaly be a `JsObject` of type `Map[String, Long]`
  - example: `{"score": 100}`

## MD5
Before running a metric, the API checks the MD5 checksum of the formula code...

In SQL you can check it yourself like this:

```sql
-- vérification des checksum MD5
SELECT id, created_by, formula_name, md5(formula_code) , code_checksum FROM scoring_metrics ;

-- exemple de mise à jour du checksum MD5
UPDATE scoring_metrics SET code_checksum = '6886ed71568a7f95eb21249cdaa999f5' WHERE id = '00d43222-d726-43ad-ab85-96cfe00cc489' ;
```


## Testing a metric

The metric development can take place in the sbt project in this src/ folder.

Just copy your metric formula and run it:
```
sbt compile
sbt run "{yourJsonInputHere}"
```
