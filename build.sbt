name := "minimetric"

version := "0.1"

scalaVersion := "2.11.8"

libraryDependencies +=  "com.typesafe.play"          % "play-json_2.11"                              % "2.4.11"
libraryDependencies +=  "com.typesafe.play"          % "play_2.11"                                   % "2.4.11"
libraryDependencies +=  "com.typesafe.play"          % "play-datacommons_2.11"                       % "2.4.11"
libraryDependencies +=  "com.typesafe.play"          % "play-functional_2.11"                        % "2.4.11"
libraryDependencies +=  "joda-time"                  % "joda-time"                                   % "2.9"
libraryDependencies +=  "org.joda"                   % "joda-convert"                                % "1.9.2"
libraryDependencies +=  "com.fasterxml.jackson.core" % "jackson-core"                                % "2.9.4"
libraryDependencies +=  "com.fasterxml.jackson.core" % "jackson-databind"                            % "2.9.4"
libraryDependencies +=  "com.fasterxml.jackson.core" % "jackson-annotations"                         % "2.9.4"